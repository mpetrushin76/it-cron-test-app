export type Filter = { Id: string; Name: string };

export type Case = {
  Id: string;
  Image: string;
  Title: string;
  CaseColor: string;
  FeaturesTitle: string;
  FriendlyURL: string;
  Filters: Filter[];
};

export type CasePayload = {
  Error: null;
  Data: Case[];
};
