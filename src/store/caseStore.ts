import axios from "axios";
import { createEffect, createEvent, createStore } from "effector";
import { CasePayload } from "../utils/entity";

export const $cases = createStore<CasePayload>({ Error: null, Data: [] });
export const setCases = createEvent<CasePayload>();

$cases.on(setCases, (_, cases) => cases);

export const getCases = async () => {
  const data = await (
    await axios.get("https://services.it-cron.ru/api/cases")
  ).data;
  console.log(data)
  setCases(data);
};
