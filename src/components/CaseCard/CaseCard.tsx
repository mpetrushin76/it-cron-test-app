import React from "react";
import styles from "./style.module.scss";
type Props = {
  backgroundColor: string;
  image: string;
  size?: "medium" | "big";
  title: string;
  filters: string;
};

export const CaseCard = (props: Props) => {
  const hexToRgb = (hex: string) => {
    let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result
      ? {
          r: parseInt(result[1], 16),
          g: parseInt(result[2], 16),
          b: parseInt(result[3], 16),
        }
      : null;
  };
  const hexInverse = (hex: string) => {
    let rgb = hexToRgb(hex);
    if (rgb) {
      let luminance = 0.2126 * rgb["r"] + 0.7152 * rgb["g"] + 0.0722 * rgb["b"];
      return luminance < 140 ? "#ffffff" : "#000000";
    }
    return "#ffffff";
  };

  return (
    <div
      className={`${styles.caseCard} ${
        props.size
          ? props.size == "big"
            ? styles.caseBig
            : styles.caseMedium
          : ""
      }`}
      style={{ backgroundColor: `#${props.backgroundColor}` }}
    >
      <img src={props.image} alt="" />
      <div className={styles.description}>
        <div
          style={{ color: hexInverse(`#${props.backgroundColor}`) }}
          className={styles.title}
        >
          {props.title}
        </div>
        <div
          style={{ color: hexInverse(`#${props.backgroundColor}`) }}
          className={styles.text}
        >
          {props.filters}
        </div>
      </div>
    </div>
  );
};
