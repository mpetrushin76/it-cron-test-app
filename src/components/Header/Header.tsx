import React from "react";

import { Flex, Layout } from "antd";
import { Icon } from "../Icon";
import { LogoSvg, MenuBtn } from "../../assets/images";
import styles from "./style.module.scss";
import { useMenuToggle } from "../Menu/MenuProvider";
type Props = {};

export const Header = (props: Props) => {
  const toggle = useMenuToggle();
  return (
    <Layout.Header className={styles.header}>
      <div className={styles.headerWrapper}>
        <a href="/">
          <Icon>
            <LogoSvg />
          </Icon>
        </a>
        <div className={styles.content}>
          <a className={`${styles.headerLink} ${styles.headerLinkActive}`}>
            Кейсы
          </a>
          <a className={styles.headerLink}>Компания</a>
          <a className={styles.headerLink}>Услуги</a>
          <a className={styles.headerLink}>Контакты</a>
        </div>
        <a onClick={toggle} className={styles.menuBtn} >
          <Icon>
            <MenuBtn />
          </Icon>
        </a>
      </div>
    </Layout.Header>
  );
};
