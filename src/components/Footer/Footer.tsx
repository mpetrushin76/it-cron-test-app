import { Flex, Layout } from "antd";
import styles from "./style.module.scss";
import { Icon } from "../Icon";
import { Telegram } from "../../assets/images";
type Props = {};

export const Footer = (props: Props) => {
  return (
    <Layout.Footer className={styles.footer}>
      <div className={styles.footerWrapper}>
        <Flex gap={window.innerWidth < 600 ? 30 : 60} align="center" vertical>
          <div className={styles.footerTitle}>
            Стать клиентом или партнером!
          </div>
          <div className={styles.line} />
          <div className={styles.mail}>hello@it-cron.ru</div>
        </Flex>
        <div className={styles.additionalInfo}>
          <div className={styles.info}>
            <div className={styles.region}>Россия, Москва</div>
            <div className={styles.address}>119330, ул. Мосфильмовская, 35</div>
            <div className={styles.phone}>+7 (495) 006-13-57</div>
          </div>
          <div className={styles.btn}>Оставить заявку</div>
          <div className={styles.social}>
            <div className={styles.label}>Связаться через</div>
            <div className={styles.btnSoc}>
              <Icon>
                <Telegram />
              </Icon>
            </div>
          </div>
        </div>
      </div>
    </Layout.Footer>
  );
};
