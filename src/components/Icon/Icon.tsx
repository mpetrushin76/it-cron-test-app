import { ReactNode } from "react";
import styles from "./style.module.scss";

type Props = { children: ReactNode };

export const Icon = (props: Props) => {
  return <i className={styles.icon}>{props.children}</i>;
};
