import React, { useContext, useState } from "react";
const MenuContext = React.createContext(false);
const MenuToggleContext = React.createContext(() => {});


export const useMenuShow = () => {
    return useContext(MenuContext);
  };
  
  export const useMenuToggle = () => {
    return useContext(MenuToggleContext);
  };

type Props = { children: React.ReactNode };

export const MenuProvider = (props: Props) => {
  const [show, setShow] = useState(false);

  const toggleShow = () => setShow((prev) => !prev);
  return (
    <MenuContext.Provider value={show}>
      <MenuToggleContext.Provider value={toggleShow}>
        {props.children}
      </MenuToggleContext.Provider>
    </MenuContext.Provider>
  );
};
