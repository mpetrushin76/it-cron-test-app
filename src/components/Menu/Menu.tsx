import React from "react";
import styles from "./style.module.scss";
import { Icon } from "../Icon";
import { CloseIcon, MenuLogo } from "../../assets/images";
import { useMenuShow, useMenuToggle } from "./MenuProvider";
type Props = {};

export const Menu = (props: Props) => {
  const show = useMenuShow();
  const toggle = useMenuToggle();

  if (!show) return <></>;

  return (
    <div className={styles.menu}>
      <div className={styles.menuWrapper}>
        <div className={styles.navBlock}>
          <Icon>
            <MenuLogo />
          </Icon>
          <div onClick={toggle} className={styles.closeBtnMob}>
            <Icon>
              <CloseIcon />
            </Icon>
          </div>

          <div className={styles.menuList}>
            <a className={styles.menuItem} href="/">
              Кейсы
            </a>
            <a className={styles.menuItem} href="/">
              Компания
            </a>
            <a className={styles.menuItem} href="/">
              Услуги
            </a>
            <a className={styles.menuItem} href="/">
              Контакты
            </a>
          </div>
          <a className={styles.mail} href="/">
            hello@it-cron.ru
          </a>
        </div>
        <div className={styles.socBlock}>
          <Icon>
            <div onClick={toggle}>
              <CloseIcon />
            </div>
          </Icon>

          <a className={styles.socLink}>Telegram</a>
          <div className={styles.requestBtn}>Оставить заявку</div>
          <a className={styles.mailForMob} href="/">
            hello@it-cron.ru
          </a>
        </div>
      </div>
    </div>
  );
};
