import { useState } from "react";
import styles from "./style.module.scss";
import { useFilterShow } from "../FilterBtn/FilterProvider";
type Props = {};

const TEMP_DATA_FILTERS = [
  {
    title: "Отрасль",
    items: [
      "BI-отчеты",
      "Гос. заказ",
      "BI-отчеты",
      "Гос. заказ",
      "Энергетика",
      "Образование",
      "Финтех",
      "Образ жизни",
      "Медицина",
      "E-commerce",
      "Безопасность",
      "Развлечения",
      "Картография",
    ],
  },
  {
    title: "Платформы",
    items: [
      "Android",
      "Chat-system",
      "Telegram",
      "Backend",
      "Windows",
      "Панель управления",
      "Web",
      "iOS",
    ],
  },
  {
    title: "Услуги",
    items: [
      "Релиз",
      "Сопровождение",
      "Дизайн",
      "Тестирование",
      "Разработка",
      "Предпроектная деятельность",
      "Аналитика",
      "Приемка",
    ],
  },
  {
    title: "Языки",
    items: [
      "Node.JS",
      "Java",
      "Xamarin",
      "Wordpress",
      "Drupal 8, Angular",
      "ASP.NET Core, Azure",
      "MS Power BI",
      "1C-Битрикс",
      "Objective-C",
      "C#, UWP",
      "Python",
      "PHP",
      "Kotlin",
      "MODX",
      "Swift",
      "PhoneGap",
      "React",
      "Java, Kotlin",
      "Cordova",
      "C++",
    ],
  },
];

export const CaseFilters = (props: Props) => {
  const showFilter = useFilterShow();
  return (
    <div
      className={`${styles.caseFilters}`}
    >
      <div className={styles.caseFiltersRect} />
      <div
        className={`${styles.caseFiltersContent}  ${
          showFilter && styles.active
        }`}
      >
        {TEMP_DATA_FILTERS.map((category, index) => (
          <div key={`category_filter_${index}`} className={styles.filterCatrgory}>
            <div className={styles.filterCatrgoryTitle}>{category.title}</div>
            {category.items.map((item, index) => (
              <div key={`filter_${index}`} className={styles.filterItem}>{item}</div>
            ))}
          </div>
        ))}
      </div>
    </div>
  );
};
