import React, { useState } from "react";
import styles from "./style.module.scss";
import { useFilterShow, useFilterToggle } from "./FilterProvider";
type Props = {};

export const FilterBtn = (props: Props) => {
  const toggle = useFilterToggle();
  const showFilter = useFilterShow();

  return (
    <div
      onClick={() => toggle()}
      className={styles.filterBtn}
    >
      <div className={`${styles.btnRect} ${showFilter && styles.active}`} />
      <div className={`${styles.line} ${showFilter && styles.active}`} />
      <div className={`${styles.text} ${showFilter && styles.active}`}>Фильтры</div>
    </div>
  );
};
