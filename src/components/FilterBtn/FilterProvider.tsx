import React, { useContext, useState } from "react";
const FilterContext = React.createContext(false);
const FilterToggleContext = React.createContext(() => {});

export const useFilterShow = () => {
  return useContext(FilterContext);
};

export const useFilterToggle = () => {
  return useContext(FilterToggleContext);
};

type Props = { children: React.ReactNode };

export const FiltertProvider = (props: Props) => {
  const [show, setShow] = useState(false);

  const toggleShow = () => setShow((prev) => !prev);

  return (
    <FilterContext.Provider value={show}>
      <FilterToggleContext.Provider value={toggleShow}>
        {props.children}
      </FilterToggleContext.Provider>
    </FilterContext.Provider>
  );
};
