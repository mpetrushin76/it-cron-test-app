import React from "react";
import styles from "./style.module.scss";
import {
  CaseContainer,
  CaseFilters,
  FilterBtn,
  Footer,
} from "../../components";
import { FiltertProvider } from "../../components/FilterBtn/FilterProvider";

type Props = {};

const CasePage = (props: Props) => {
  return (
    <div className={styles.casePage}>
      <div className={styles.fitstRect} />
      <FiltertProvider>
        <div className={styles.header}>
          <h1 className={styles.title}>Кейсы</h1>
          <FilterBtn />
        </div>
        <CaseFilters />
      </FiltertProvider>
      <CaseContainer />
    </div>
  );
};

export default CasePage;
