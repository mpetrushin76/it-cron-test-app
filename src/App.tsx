import "./App.css";
import CasePage from "./page/CasePage/CasePage";
import { ConfigProvider, Layout } from "antd";
import { Footer, Header, Menu } from "./components";
import { MenuProvider } from "./components/Menu/MenuProvider";
import { useEffect } from "react";
import { getCases } from "./store/caseStore";
function App() {
  useEffect(() => {
    getCases();
  }, []);
  return (
    <ConfigProvider
      theme={{ token: { colorPrimary: "#fb6542" }, hashed: false }}
    >
      <Layout className="main-layout">
        <MenuProvider>
          <Header />
          <Menu />
        </MenuProvider>

        <Layout.Content>
          <CasePage />
        </Layout.Content>
        <Footer />
      </Layout>
    </ConfigProvider>
  );
}

export default App;
