export { ReactComponent as LogoSvg } from "./Logo.svg";
export { ReactComponent as MenuBtn } from "./MenuBtn.svg";
export { ReactComponent as Telegram } from "./Telegram.svg";
export { ReactComponent as MenuLogo } from "./MenuLogo.svg";
export { ReactComponent as CloseIcon } from "./CloseIcon.svg";
